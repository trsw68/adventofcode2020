package dev.xengil.aoc.day11;

@FunctionalInterface
public interface SeatRule {

    SeatLayoutPosition apply(int x, int y, SeatLayoutPosition pos);

    default boolean modifies(int x, int y, SeatLayoutPosition pos) {
        return pos != this.apply(x, y, pos);
    }
}
