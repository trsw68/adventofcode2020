package dev.xengil.aoc.day11.b;

import dev.xengil.aoc.day11.SeatRule;
import dev.xengil.aoc.day11.SeatLayoutPosition;
import dev.xengil.aoc.day11.WaitingArea;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static dev.xengil.aoc.day11.SeatLayoutPosition.EMPTY_SEAT;
import static dev.xengil.aoc.day11.SeatLayoutPosition.OCCUPIED_SEAT;

public class D11B {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD11"));

        SeatLayoutPosition[][] seatLayout = input
            .stream()
            .map(line -> line
                .chars()
                .mapToObj(c -> SeatLayoutPosition.of((char) c))
                .toArray(SeatLayoutPosition[]::new)
            )
            .toArray(SeatLayoutPosition[][]::new)
            ;

        WaitingArea waitingArea = new WaitingArea(seatLayout);

        Set<SeatRule> ruleSet = getRuleSet(waitingArea);

        do {
//            waitingArea.printWaitingArea();
        } while (waitingArea.nextRound(ruleSet));

//        waitingArea.printWaitingArea();

        System.out.println(waitingArea.getOccupiedSeatCount());
    }

    private static Set<SeatRule> getRuleSet(WaitingArea waitingArea) {
        return new HashSet<>() {{
            add((x, y, pos) ->
                pos == EMPTY_SEAT &&
                    waitingArea.getAdjacentSeats(x, y, true).stream().noneMatch(p -> p == OCCUPIED_SEAT) ?
                    OCCUPIED_SEAT :
                    pos
            );
            add((x, y, pos) ->
                pos == OCCUPIED_SEAT &&
                    waitingArea.getAdjacentSeats(x, y, true).stream().filter(p -> p == OCCUPIED_SEAT).count() >= 5 ?
                    EMPTY_SEAT :
                    pos
            );
        }};
    }
}
