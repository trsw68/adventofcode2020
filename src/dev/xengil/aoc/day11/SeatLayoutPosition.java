package dev.xengil.aoc.day11;

import java.util.Arrays;

public enum SeatLayoutPosition {
    FLOOR('.'),
    EMPTY_SEAT('L'),
    OCCUPIED_SEAT('#');

    public static SeatLayoutPosition of(char c) {
        return Arrays.stream(values()).filter(pos -> pos.c == c).findFirst().orElse(null);
    }

    public final char c;

    SeatLayoutPosition(char c) {
        this.c = c;
    }
}
