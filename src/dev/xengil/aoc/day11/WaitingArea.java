package dev.xengil.aoc.day11;

import java.util.*;
import java.util.function.Predicate;

import static dev.xengil.aoc.day11.SeatLayoutPosition.*;

public class WaitingArea {

    private SeatLayoutPosition[][] seatLayout;

    public WaitingArea(SeatLayoutPosition[][] seatLayout) {
        this.seatLayout = seatLayout;
    }

    public boolean nextRound(Set<SeatRule> ruleSet) {
        SeatLayoutPosition[][] nextSeatLayout = new SeatLayoutPosition[this.seatLayout.length][0];

        boolean stateChanges = false;

        for (int y = 0; y < this.seatLayout.length; y++) {
            nextSeatLayout[y] = new SeatLayoutPosition[this.seatLayout[y].length];

            for (int x = 0; x < this.seatLayout[y].length; x++) {
                SeatLayoutPosition seatLayoutPosition = this.seatLayout[y][x];

                final int posX = x, posY = y;

                nextSeatLayout[y][x] = ruleSet
                    .stream()
                    .filter(rule -> rule.modifies(posX, posY, seatLayoutPosition))
                    .findFirst()
                    .map(rule -> rule.apply(posX, posY, seatLayoutPosition))
                    .orElse(seatLayoutPosition)
                ;

                if (nextSeatLayout[y][x] != this.seatLayout[y][x]) {
                    stateChanges = true;
                }
            }
        }

        this.seatLayout = nextSeatLayout;

        return stateChanges;
    }

    public long getOccupiedSeatCount() {
        return Arrays.stream(this.seatLayout).flatMap(Arrays::stream).filter(pos -> pos == OCCUPIED_SEAT).count();
    }

    public void printWaitingArea() {
        System.out.println(this.toString());
    }

    public List<SeatLayoutPosition> getAdjacentSeats(int posX, int posY) {
        return this.getAdjacentSeats(posX, posY, false);
    }

    public List<SeatLayoutPosition> getAdjacentSeats(int posX, int posY, boolean skipFloor) {
        List<SeatLayoutPosition> adjacentSeats = new ArrayList<>();

        Predicate<Position> isValidPosition = pos ->
            pos.y >= 0 && pos.y < this.seatLayout.length &&
            pos.x >= 0 && pos.x < this.seatLayout[pos.y].length
        ;

        for (Direction direction : Direction.values()) {
            Position position = new Position(posX, posY);

            position = direction.transform(position);

            while (isValidPosition.test(position)) {
                SeatLayoutPosition seatLayoutPosition = this.getSeatLayoutPosition(position);

                if (seatLayoutPosition != FLOOR) {
                    adjacentSeats.add(seatLayoutPosition);
                    break;
                }

                if (!skipFloor) {
                    break;
                }

                position = direction.transform(position);
            }
        }

        return adjacentSeats;
    }

    private SeatLayoutPosition getSeatLayoutPosition(Position position) {
        return this.seatLayout[position.y][position.x];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (SeatLayoutPosition[] row : this.seatLayout) {
            for (SeatLayoutPosition pos : row) {
                sb.append(pos.c);
            }

            sb.append('\n');
        }

        return sb.toString();
    }

    private enum Direction {
        UP(0, -1),
        UP_RIGHT(1, -1),
        RIGHT(1, 0),
        DOWN_RIGHT(1, 1),
        DOWN(0, 1),
        DOWN_LEFT(-1, 1),
        LEFT(-1, 0),
        UP_LEFT(-1, -1);

        private final int dx, dy;

        Direction(int dx, int dy) {
            this.dx = dx;
            this.dy = dy;
        }

        private Position transform(Position position) {
            return position.transform(this.dx, this.dy);
        }
    }

    private static class Position {

        private final int x, y;

        private Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Position transform(int x, int y) {
            return new Position(this.x + x, this.y + y);
        }
    }
}
