package dev.xengil.aoc.day10;

import java.util.Collection;

public class JoltageAdapter implements Comparable<JoltageAdapter> {

    public static JoltageAdapter generateDeviceJoltageAdapter(Collection<JoltageAdapter> joltageAdapters) {
        return new JoltageAdapter(
            3 + joltageAdapters.stream().mapToInt(JoltageAdapter::getOutputJoltage).max().orElse(0)
        );
    }

    //------------------------------------------------------------------------------------------------------------------

    private final int outputJoltage;

    public JoltageAdapter(int outputJoltage) {
        this.outputJoltage = outputJoltage;
    }

    public int getOutputJoltage() {
        return this.outputJoltage;
    }

    public boolean canBePluggedInto(JoltageAdapter other) {
        int minJoltage = this.outputJoltage - 3;
        int maxJoltage = this.outputJoltage - 1;

        return other.outputJoltage >= minJoltage && other.outputJoltage <= maxJoltage;
    }

    @Override
    public int compareTo(JoltageAdapter other) {
        return this.outputJoltage - other.outputJoltage;
    }

    @Override
    public String toString() {
        return String.format("JA(%d)", this.outputJoltage);
    }
}
