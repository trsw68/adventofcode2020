package dev.xengil.aoc.day10.a;

import dev.xengil.aoc.day10.JoltageAdapter;
import dev.xengil.aoc.day10.JoltageAdapterChain;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class D10A {

    public static void main(String[] args) throws IOException {
        List<JoltageAdapter> joltageAdapters = InputConverter
            .getInstance()
            .convertInput(s -> new JoltageAdapter(Integer.parseInt(s)), Path.of("rsc/inputD10"))
        ;

        JoltageAdapter chargingOutlet = new JoltageAdapter(0);
        JoltageAdapter deviceBuiltIn  = JoltageAdapter.generateDeviceJoltageAdapter(joltageAdapters);

        joltageAdapters.add(chargingOutlet);
        joltageAdapters.add(deviceBuiltIn);

        int[] joltageDifferences = new JoltageAdapterChain(joltageAdapters).getJoltageDifferences();

        long oneJoltDifferences   = Arrays.stream(joltageDifferences).filter(i -> i == 1).count();
        long threeJoltDifferences = Arrays.stream(joltageDifferences).filter(i -> i == 3).count();

        System.out.println(oneJoltDifferences * threeJoltDifferences);
    }
}
