package dev.xengil.aoc.day10.b;

import dev.xengil.aoc.day10.JoltageAdapter;
import dev.xengil.aoc.day10.JoltageAdapterChain;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class D10B {

    public static void main(String[] args) throws IOException {
        List<JoltageAdapter> joltageAdapters = InputConverter
            .getInstance()
            .convertInput(s -> new JoltageAdapter(Integer.parseInt(s)), Path.of("rsc/inputD10"))
        ;

        JoltageAdapter chargingOutlet = new JoltageAdapter(0);
        JoltageAdapter deviceBuiltIn  = JoltageAdapter.generateDeviceJoltageAdapter(joltageAdapters);

        joltageAdapters.add(chargingOutlet);
        joltageAdapters.add(deviceBuiltIn);

        long possibleArrangements = new JoltageAdapterChain(joltageAdapters).getPossibleArrangements();

        System.out.println(possibleArrangements);
    }
}
