package dev.xengil.aoc.day10;

import java.util.*;

public class JoltageAdapterChain {

    private final List<JoltageAdapter> joltageAdapters;

    public JoltageAdapterChain(Collection<JoltageAdapter> joltageAdapters) {
        assert Set.copyOf(joltageAdapters).size() == joltageAdapters.size() : "Joltage adapters must be unique";

        this.joltageAdapters = new ArrayList<>(joltageAdapters);
        this.joltageAdapters.sort(Comparator.naturalOrder());
    }

    public List<JoltageAdapter> buildChain() {
        return List.copyOf(this.joltageAdapters);
    }

    public int[] getJoltageDifferences() {
        List<JoltageAdapter> joltageAdapterChain = this.buildChain();
        int[] joltageDifferences = new int[joltageAdapterChain.size() - 1];

        for (int i = 0; i < joltageDifferences.length; i++) {
            JoltageAdapter curr = joltageAdapterChain.get(i);
            JoltageAdapter next = joltageAdapterChain.get(i + 1);
            int            diff = next.getOutputJoltage() - curr.getOutputJoltage();

            assert next.canBePluggedInto(curr);

            joltageDifferences[i] = diff;
        }

        return joltageDifferences;
    }

    public long getPossibleArrangements() {
        return this.generatePossibleChains(this.joltageAdapters, new HashMap<>());
    }

    private long generatePossibleChains(List<JoltageAdapter> remaining, Map<List<JoltageAdapter>, Long> memory) {
        if (memory.containsKey(remaining)) {
            return memory.get(remaining);
        }

        System.out.println("remaining: " + remaining.size());

        List<JoltageAdapter> tmp = new ArrayList<>(remaining);

        JoltageAdapter last = tmp.remove(0);

        if (tmp.isEmpty()) {
            return 1;
        }

        // without skipping the one in between
        long possibleChains = this.generatePossibleChains(tmp, memory);

        memory.put(List.copyOf(tmp), possibleChains);

        while (tmp.size() >= 2 && tmp.get(1).canBePluggedInto(last)) { // skip the one(s) in between, if possible
            tmp.remove(0);

            long res = this.generatePossibleChains(tmp, memory);

            memory.put(new ArrayList<>(tmp), res);

            possibleChains += res;
        }

        return possibleChains;
    }
}
