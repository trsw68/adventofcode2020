package dev.xengil.aoc.day04;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Predicate;

public enum PassportField {
    BIRTH_YEAR("byr", true, validateInt(1920, 2002)),
    ISSUE_YEAR("iyr", true, validateInt(2010, 2020)),
    EXPIRATION_YEAR("eyr", true, validateInt(2020, 2030)),
    HEIGHT("hgt", true, data -> {
        if (data.matches("\\d+(cm|in)")) {
            String value = data.substring(0, data.length() - 2);

            return data.contains("cm")
                ? validateInt(150, 193).test(value) // cm
                : validateInt(59, 76).test(value)   // in
            ;
        }

        return false;
    }),
    HAIR_COLOR("hcl", true, data -> data.matches("#(([0-9]|[a-f]){6})")),
    EYE_COLOR("ecl", true, data -> data.matches("amb|blu|brn|gry|grn|hzl|oth")),
    PASSPORT_ID("pid", true, data -> data.matches("\\d{9}")),
    COUNTRY_ID("cid", false, __ -> true);

    private static Predicate<String> validateInt(int min, int max) {
        return data -> data.matches("\\d+")
            && Integer.parseInt(data) >= min
            && Integer.parseInt(data) <= max
        ;
    }

    public static PassportField fromString(String representation) {
        return Arrays
            .stream(PassportField.values())
            .filter(passportField -> passportField.representation.equals(representation))
            .findFirst()
            .orElse(null)
        ;
    }

    public static boolean isValidPassport(PassportField ...passport) {
        return Arrays
            .stream(values())
            .filter(PassportField::isRequired)
            .allMatch(requiredField ->
                Arrays.stream(passport).anyMatch(passportField -> passportField == requiredField)
            )
        ;
    }

    public static boolean isValidPassport(Map<PassportField, String> passport) {
        return Arrays
            .stream(values())
            .filter(PassportField::isRequired)
            .allMatch(requiredField ->
                passport.get(requiredField) != null && requiredField.validate(passport.get(requiredField))
            )
        ;
    }

    private final String representation;

    private final boolean required;

    private final Predicate<String> validator;

    PassportField(String representation, boolean required, Predicate<String> validator) {
        this.representation = representation;
        this.required       = required;
        this.validator      = validator;
    }

    public boolean isRequired() {
        return this.required;
    }

    public boolean validate(String data) {
        return this.validator.test(data);
    }
}
