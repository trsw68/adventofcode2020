package dev.xengil.aoc.day04.b;

import dev.xengil.aoc.day04.PassportField;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static dev.xengil.aoc.day04.a.D4A.parsePassports;

public class D4B {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD4"));

        List<Map<PassportField, String>> passports = parsePassports(input);

        System.out.println(countValidPassports(passports));
    }

    private static long countValidPassports(List<Map<PassportField, String>> passports) {
        return passports.stream().filter(PassportField::isValidPassport).count();
    }
}
