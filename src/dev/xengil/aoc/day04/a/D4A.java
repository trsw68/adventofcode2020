package dev.xengil.aoc.day04.a;

import dev.xengil.aoc.day04.PassportField;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class D4A {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD4"));

        List<PassportField[]> passports = parsePassports(input)
            .stream()
            .map(map -> map.keySet().toArray(PassportField[]::new))
            .collect(Collectors.toList())
        ;

        System.out.println(countValidPassports(passports));
    }

    public static List<Map<PassportField, String>> parsePassports(List<String> input) {
        List<Map<PassportField, String>> passports      = new ArrayList<>();
        Map<PassportField, String>       passportFields = new HashMap<>();

        for (String line : input) {
            if (line.isBlank()) { // save passport fields to passport
                passports.add(Map.copyOf(passportFields));

                passportFields.clear();
            } else {
                Arrays
                    .stream(line.split(" "))
                    .map(field -> field.split(":"))
                    .forEach(data -> passportFields.put(PassportField.fromString(data[0]), data[1]))
                ;
            }
        }

        return passports;
    }

    private static long countValidPassports(List<PassportField[]> passports) {
        return passports.stream().filter(PassportField::isValidPassport).count();
    }
}
