package dev.xengil.aoc.day13;

public class Shuttle {

    public final int ID;

    public final int POSITION;

    public Shuttle(int id) {
        this(id, -1);
    }

    public Shuttle(int id, int position) {
        this.ID       = id;
        this.POSITION = position;
    }
}
