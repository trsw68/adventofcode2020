package dev.xengil.aoc.day13;

import java.util.Comparator;
import java.util.List;

public class ShuttleSearcher {

    private final List<Shuttle> SHUTTLES;

    public ShuttleSearcher(List<Shuttle> shuttles) {
        this.SHUTTLES = List.copyOf(shuttles);
    }

    public Result find(int earliestDeparture) {
        return this.SHUTTLES
            .stream()
            .map(shuttle -> new Result(shuttle, shuttle.ID - (earliestDeparture % shuttle.ID)))
            .min(Comparator.comparingInt(result -> result.WAIT_TIME))
            .orElseThrow()
        ;
    }

    public static class Result {

        public final Shuttle SHUTTLE;

        public final int WAIT_TIME;

        private Result(Shuttle shuttle, int waitTime) {
            this.SHUTTLE   = shuttle;
            this.WAIT_TIME = waitTime;
        }

        @Override
        public String toString() {
            return String.format("Result{ Shuttle#%d, Wait time: %d }", this.SHUTTLE.ID, this.WAIT_TIME);
        }
    }
}
