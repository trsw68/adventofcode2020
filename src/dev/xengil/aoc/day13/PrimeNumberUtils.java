package dev.xengil.aoc.day13;

import java.util.*;
import java.util.stream.Collectors;

public class PrimeNumberUtils {

    public static long lcm(long ...nums) {
        Comparator<Object> comparator = Comparator
            .comparingLong(primeFactor -> ((PrimeFactor) primeFactor).EXPONENT)
            .reversed()
        ;

        @SuppressWarnings("RedundantStreamOptionalCall")
        Set<PrimeFactor> primeFactors = Arrays
            .stream(nums)
            .mapToObj(PrimeNumberUtils::getPrimeFactors)
            .flatMap(Collection::stream)
            .sorted(comparator)
            .collect(Collectors.toSet())
        ;

        long lcm = primeFactors.isEmpty() ? 0 : 1;

        for (PrimeFactor primeFactor : primeFactors) {
            lcm *= Math.pow(primeFactor.PRIME_FACTOR, primeFactor.EXPONENT);
        }

        return lcm;
    }

    private static List<PrimeFactor> getPrimeFactors(long num) {
        Map<Long, Long> primeFactors = new HashMap<>();

        outer:
        while (num > 1) {
            for (long i = 2; i <= num; i++) {
                if (num % i == 0) { // i is a prime factor of num
                    primeFactors.computeIfPresent(i, (k, v) -> ++v);
                    primeFactors.putIfAbsent(i, 1L);

                    num /= i; // "remove" the prime factor found from num

                    continue outer;
                }
            }
        }

        return primeFactors
            .keySet()
            .stream()
            .map(primeFactor -> new PrimeFactor(primeFactor, primeFactors.get(primeFactor)))
            .collect(Collectors.toList())
        ;
    }

    private PrimeNumberUtils() { }

    private static class PrimeFactor {

        private final long PRIME_FACTOR;

        private final long EXPONENT;

        public PrimeFactor(long primeFactor, long exponent) {
            this.PRIME_FACTOR = primeFactor;
            this.EXPONENT     = exponent;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof PrimeFactor && ((PrimeFactor) obj).PRIME_FACTOR == this.PRIME_FACTOR;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.PRIME_FACTOR);
        }

        @Override
        public String toString() {
            return String.format("%d^%d", this.PRIME_FACTOR, this.EXPONENT);
        }
    }
}
