package dev.xengil.aoc.day13;

import java.util.ArrayList;
import java.util.List;

import static dev.xengil.aoc.day13.PrimeNumberUtils.lcm;

public class ShuttleAnalyzer {

    private final List<Shuttle> SHUTTLES;

    public ShuttleAnalyzer(List<Shuttle> shuttles) {
        assert shuttles.stream().noneMatch(shuttle -> shuttle.POSITION == -1) : "Shuttle positions must be set";

        this.SHUTTLES = new ArrayList<>(shuttles);
    }

    public long findSubsequentDepartures() {
        long resultTime       = 0;
        long currentStartTime = this.SHUTTLES.get(0).ID;

        for (int i = 1; i < this.SHUTTLES.size(); i++) {
            Shuttle shuttle = this.SHUTTLES.get(i);

            /*
             * Find a time that is a valid departure time for the shuttle
             * including our shuttle position (= time offset).
             */
            while ((resultTime + shuttle.POSITION) % shuttle.ID != 0) {
                resultTime += currentStartTime;
            }

            currentStartTime = lcm(currentStartTime, shuttle.ID);
        }

        return resultTime;
    }
}
