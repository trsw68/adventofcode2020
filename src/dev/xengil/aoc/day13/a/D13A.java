package dev.xengil.aoc.day13.a;

import dev.xengil.aoc.day13.Shuttle;
import dev.xengil.aoc.day13.ShuttleSearcher;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class D13A {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD13"));

        assert input.size() == 2;

        int earliestDeparture = Integer.parseInt(input.get(0));

        List<Shuttle> shuttles = Arrays
            .stream(input.get(1).split(","))
            .filter(s -> !s.equals("x"))
            .mapToInt(Integer::parseInt)
            .mapToObj(Shuttle::new)
            .collect(Collectors.toList())
        ;

        ShuttleSearcher shuttleSearcher = new ShuttleSearcher(shuttles);

        ShuttleSearcher.Result result = shuttleSearcher.find(earliestDeparture);

        System.out.println(result);
        System.out.println(result.SHUTTLE.ID * result.WAIT_TIME);
    }
}
