package dev.xengil.aoc.day13.b;

import dev.xengil.aoc.day13.Shuttle;
import dev.xengil.aoc.day13.ShuttleAnalyzer;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class D13B {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD13"));

        assert input.size() == 2;

        String[]      shuttleStrings = input.get(1).split(",");
        List<Shuttle> shuttles       = new ArrayList<>(shuttleStrings.length);

        for (int i = 0, position = 0; i < shuttleStrings.length; i++, position++) {
            String shuttleString = shuttleStrings[i];

            if (shuttleString.equals("x")) {
                continue;
            }

            shuttles.add(new Shuttle(Integer.parseInt(shuttleString), position));
        }

        ShuttleAnalyzer shuttleAnalyzer = new ShuttleAnalyzer(shuttles);

        System.out.println(shuttleAnalyzer.findSubsequentDepartures());
    }
}
