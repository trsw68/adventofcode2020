package dev.xengil.aoc.day05;

public class BoardingPass implements Comparable<BoardingPass> {

    public static BoardingPass of(String string) {
        string = toBinary(string);

        int row = Integer.parseInt(string.substring(0, 7), 2);
        int col = Integer.parseInt(string.substring(7), 2);

        return new BoardingPass(row, col);
    }

    private static String toBinary(String string) {
        assert string.matches("([FB]){7}[LR]{3}");

        StringBuilder result = new StringBuilder();

        for (char c : string.toCharArray()) {
            switch (c) {
                case 'F':
                case 'L':
                    result.append(0);
                    break;
                case 'B':
                case 'R':
                    result.append(1);
                    break;
            }
        }

        return result.toString();
    }

    public final int ROW;

    public final int COL;

    public final int SEAT_ID;

    public BoardingPass(int row, int col) {
        this.ROW     = row;
        this.COL     = col;
        this.SEAT_ID = row * 8 + col;
    }

    @Override
    public int compareTo(BoardingPass other) {
        return Integer.compare(this.SEAT_ID, other.SEAT_ID);
    }
}
