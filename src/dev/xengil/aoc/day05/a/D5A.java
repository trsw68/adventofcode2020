package dev.xengil.aoc.day05.a;

import dev.xengil.aoc.day05.BoardingPass;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;

public class D5A {

    public static void main(String[] args) throws IOException {
        List<BoardingPass> input = InputConverter.getInstance().convertInput(BoardingPass::of, Path.of("rsc/inputD5"));

        System.out.println(input.stream().max(Comparator.naturalOrder()).orElseThrow().SEAT_ID);
    }
}
