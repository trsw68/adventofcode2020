package dev.xengil.aoc.day05.b;

import dev.xengil.aoc.day05.BoardingPass;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;

public class D5B {

    public static void main(String[] args) throws IOException {
        List<BoardingPass> input = InputConverter.getInstance().convertInput(BoardingPass::of, Path.of("rsc/inputD5"));

        System.out.println(findSeatId(input));
    }

    private static int findSeatId(List<BoardingPass> boardingPasses) {
        assert boardingPasses.size() >= 2;

        BoardingPass curr;
        BoardingPass next;

        int idx = 0;

        boardingPasses.sort(Comparator.naturalOrder());

        while (idx < boardingPasses.size() - 1) {
            curr = boardingPasses.get(idx);
            next = boardingPasses.get(idx + 1);

            if (next.SEAT_ID == curr.SEAT_ID + 2) {
                return curr.SEAT_ID + 1;
            }

            idx++;
        }

        throw new IllegalArgumentException("No seat found");
    }
}
