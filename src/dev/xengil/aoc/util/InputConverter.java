package dev.xengil.aoc.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class InputConverter {

    private static InputConverter INSTANCE;

    public static InputConverter getInstance() {
        return INSTANCE == null ? (INSTANCE = new InputConverter()) : INSTANCE;
    }

    private InputConverter() { }

    public <T> List<T> convertInput(Function<? super String, T> mapper, Path pathToFile) throws IOException {
        return this.convertInput(pathToFile).stream().map(mapper).collect(Collectors.toList());
    }

    public List<String> convertInput(Path pathToFile) throws IOException {
        return Files.lines(pathToFile).collect(Collectors.toList());
    }
}
