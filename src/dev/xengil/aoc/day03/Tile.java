package dev.xengil.aoc.day03;

import java.util.ArrayList;
import java.util.List;

public enum Tile {
    TREE(true), OPEN_SQUARE(false);

    public static Tile fromChar(char c) {
        switch (c) {
            case '#':
                return TREE;
            case '.':
                return OPEN_SQUARE;
            default:
                return null;
        }
    }

    public static Tile[][] generateMap(List<String> input) {
        return input
            .stream()
            .map(String::toCharArray)
            .map(ar -> {
                List<Tile> tiles = new ArrayList<>(ar.length);

                for (char c : ar) {
                    tiles.add(Tile.fromChar(c));
                }

                return tiles.toArray(Tile[]::new);
            })
            .toArray(Tile[][]::new)
        ;
    }

    public final boolean isTree;

    Tile(boolean isTree) {
        this.isTree = isTree;
    }
}
