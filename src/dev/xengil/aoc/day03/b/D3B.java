package dev.xengil.aoc.day03.b;

import dev.xengil.aoc.day03.Slope;
import dev.xengil.aoc.day03.Tile;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public class D3B {

    public static void main(String[] args) throws IOException {
        List<String> input  = InputConverter.getInstance().convertInput(Path.of("rsc/inputD3"));
        Tile[][]     map    = Tile.generateMap(input);

        long result = Stream.of(
            new Slope(1, 1),
            new Slope(3, 1),
            new Slope(5, 1),
            new Slope(7, 1),
            new Slope(1, 2)
        ).mapToLong(slope -> countTrees(map, slope)).reduce(1, (a, b) -> a * b);

        System.out.println(result);
    }

    private static int countTrees(Tile[][] map, Slope slope) {
        int posX  = 0;
        int posY  = 0;
        int trees = 0;

        while (posY < map.length) {
            if (map[posY][posX].isTree) {
                trees++;
            }

            posX += slope.x;

            while (posX >= map[posY].length) {
                posX -= map[posY].length;
            }

            posY += slope.y;
        }

        return trees;
    }
}
