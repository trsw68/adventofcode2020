package dev.xengil.aoc.day03;

public class Slope {

    public final int x, y;

    public Slope(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
