package dev.xengil.aoc.day09.a;

import dev.xengil.aoc.day09.XMAS;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class D9A {

    public static void main(String[] args) throws IOException {
        List<Long> input = InputConverter.getInstance().convertInput(Long::parseLong, Path.of("rsc/inputD9"));

        XMAS xmas = new XMAS(input);

        System.out.println(xmas.getFirstInvalidNumber());
    }
}
