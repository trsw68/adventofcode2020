package dev.xengil.aoc.day09;

import java.util.ArrayList;
import java.util.List;

public class XMAS {

    public static final int PREAMBLE_SIZE = 25;

    private final List<Long> values;

    private final List<Long> invalidNumbers = new ArrayList<>();

    public XMAS(List<Long> values) {
        this.values = List.copyOf(values);

        for (int i = PREAMBLE_SIZE; i < this.values.size(); i++) {
            boolean valid = false;
            long number = this.values.get(i);

            outer:
            for (int j = 0; j < PREAMBLE_SIZE; j++) {
                for (int k = 0; k < PREAMBLE_SIZE; k++) {
                    if (j == k) {
                        continue;
                    }

                    if (number == this.values.get(i - j - 1) + this.values.get(i - k - 1)) {
                        valid = true;
                        break outer;
                    }
                }
            }

            if (!valid) {
                this.invalidNumbers.add(number);
            }
        }
    }

    public long getFirstInvalidNumber() {
        return this.invalidNumbers.get(0);
    }

    public long getEncryptionWeakness() {
        long invalidNumber = this.getFirstInvalidNumber();

        for (int start = 0; start < this.values.size(); start++) {
            int end = start;

            long val = this.values.get(start);
            long sum = val;

            if (val == invalidNumber) {
                continue;
            }

            for (int j = start + 1; j < this.values.size() && sum < invalidNumber; j++) {
                end = j;

                sum += this.values.get(j);
            }

            if (sum == invalidNumber) {
                List<Long> contiguousSet = this.values.subList(start, end + 1);

                long min = contiguousSet.stream().mapToLong(Long::longValue).min().orElseThrow();
                long max = contiguousSet.stream().mapToLong(Long::longValue).max().orElseThrow();

                return min + max;
            }
        }

        throw new IllegalStateException();
    }
}
