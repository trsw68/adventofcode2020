package dev.xengil.aoc.day02.b;

import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class D2B {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD2"));

        int validPasswords = 0;

        for (String s : input) {
            String[] parts = s.split("-|:? ", 4);

            int[]  positions = new int[] { Integer.parseInt(parts[0]) - 1, Integer.parseInt(parts[1]) - 1 };
            char   letter    = parts[2].charAt(0);
            String password  = parts[3];

            if (isValidPassword(positions, letter, password)) {
                validPasswords++;
            }
        }

        System.out.println(validPasswords);
    }

    private static boolean isValidPassword(int[] positions, char letter, String password) {
        char[] chars = password.toCharArray();

        return chars[positions[0]] == letter ^ chars[positions[1]] == letter;
    }
}
