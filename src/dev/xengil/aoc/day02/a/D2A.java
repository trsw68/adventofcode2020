package dev.xengil.aoc.day02.a;

import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class D2A {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD2"));

        int validPasswords = 0;

        for (String s : input) {
            String[] parts = s.split("-|:? ", 4);

            int    minCount = Integer.parseInt(parts[0]);
            int    maxCount = Integer.parseInt(parts[1]);
            char   letter   = parts[2].charAt(0);
            String password = parts[3];

            if (isValidPassword(minCount, maxCount, letter, password)) {
                validPasswords++;
            }
        }

        System.out.println(validPasswords);
    }

    private static boolean isValidPassword(int minCount, int maxCount, char letter, String password) {
        int count = 0;

        for (char c : password.toCharArray()) {
            if (c == letter) {
                count++;
            }
        }

        return minCount <= count && count <= maxCount;
    }
}
