package dev.xengil.aoc.day08.a;

import dev.xengil.aoc.day08.BootCode;
import dev.xengil.aoc.day08.BootCodeRunner;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static dev.xengil.aoc.day08.BootCodeRunner.DUPLICATE_FOUND;

public class D8A {

    public static void main(String[] args) throws IOException {
        List<BootCode.Instruction> input = InputConverter
            .getInstance()
            .convertInput(BootCode.Parser.getINSTANCE()::parseInstructionFromString, Path.of("rsc/inputD8"))
        ;

        BootCodeRunner bootCodeRunner = new BootCodeRunner(input);

        int exitCode;

        do {
            exitCode = bootCodeRunner.step();
        } while (exitCode != DUPLICATE_FOUND);

        System.out.println(bootCodeRunner.getAccumulator());
    }
}
