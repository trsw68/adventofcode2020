package dev.xengil.aoc.day08;

public class BootCode {

    public static class Instruction {

        public Operation operation;

        public int argument;

        public Instruction(Operation operation, int argument) {
            this.operation = operation;
            this.argument  = argument;
        }

        @Override
        public String toString() {
            return String.format("I %s %d I", this.operation, this.argument);
        }
    }

    enum Operation {
        ACC,
        JMP,
        NOP
    }

    public static class Parser {

        private static final Parser INSTANCE = new Parser();

        public static Parser getINSTANCE() {
            return INSTANCE;
        }

        public Instruction parseInstructionFromString(String string) {
            String[] parts = string.split(" ", 2);

            Operation operation = Operation.valueOf(parts[0].toUpperCase());
            int       argument  = Integer.parseInt(parts[1]);

            return new Instruction(operation, argument);
        }

        private Parser() { }
    }

    private BootCode() { }
}
