package dev.xengil.aoc.day08.b;

import dev.xengil.aoc.day08.BootCode;
import dev.xengil.aoc.day08.BootCodeRunner;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static dev.xengil.aoc.day08.BootCodeRunner.DUPLICATE_FOUND;
import static dev.xengil.aoc.day08.BootCodeRunner.PROGRAM_COMPLETED;

public class D8B {

    public static void main(String[] args) throws IOException {
        List<BootCode.Instruction> input = InputConverter
            .getInstance()
            .convertInput(BootCode.Parser.getINSTANCE()::parseInstructionFromString, Path.of("rsc/inputD8"))
        ;

        BootCodeRunner bootCodeRunner = new BootCodeRunner(input);

        int exitCode;

        while ((exitCode = bootCodeRunner.step()) != PROGRAM_COMPLETED) {
            if (exitCode == DUPLICATE_FOUND) {
                bootCodeRunner.tryFix();
            }
        }

        System.out.println(bootCodeRunner.getAccumulator());
    }
}
