package dev.xengil.aoc.day08;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static dev.xengil.aoc.day08.BootCode.Operation.JMP;
import static dev.xengil.aoc.day08.BootCode.Operation.NOP;

public class BootCodeRunner {

    public static final int PROGRAM_COMPLETED =  0;
    public static final int STEP_COMPLETED    =  1;
    public static final int DUPLICATE_FOUND   = -1;

    //-------------------------------------------------------------------------

    private int accumulator;

    /**
     * Points to the next instructions position.
     */
    private int programCounter;

    private Integer previousFix;

    private final List<BootCode.Instruction> possiblyFaultyInstructions;

    private final List<BootCode.Instruction> instructions;

    private final List<Integer> history = new ArrayList<>();

    public BootCodeRunner(List<BootCode.Instruction> instructions) {
        this.instructions               = List.copyOf(instructions);
        this.possiblyFaultyInstructions = instructions
            .stream()
            .filter(instruction -> instruction.operation == JMP || instruction.operation == NOP)
            .collect(Collectors.toList())
        ;
    }

    public int step() {
        if (this.programCounter == this.instructions.size()) { // end of program
            return PROGRAM_COMPLETED;
        } else if (this.history.contains(this.programCounter)) {
            return DUPLICATE_FOUND;
        }

        BootCode.Instruction instruction = this.getCurrentInstruction();

        this.history.add(this.programCounter);

        switch (instruction.operation) {
            case ACC:
                this.accumulator += instruction.argument;
                this.programCounter++;
                break;
            case JMP:
                this.programCounter += instruction.argument;
                break;
            case NOP:
                this.programCounter++;
                break;
            default:
                throw new IllegalArgumentException("Unknown Operation");
        }

        return STEP_COMPLETED;
    }

    public void tryFix() {
        int currentFix = 0;

        if (this.previousFix != null) {
            this.swapOp(this.possiblyFaultyInstructions.get(this.previousFix));

            currentFix = this.previousFix + 1;
        }

        BootCode.Instruction instruction = this.possiblyFaultyInstructions.get(currentFix);

        this.swapOp(instruction);

        // reset program state
        this.programCounter = 0;
        this.accumulator    = 0;
        this.history.clear();

        // save backtracking progress
        this.previousFix = currentFix;
    }

    public int getAccumulator() {
        return this.accumulator;
    }

    private BootCode.Instruction getCurrentInstruction() {
        return this.instructions.get(this.programCounter);
    }

    private void swapOp(BootCode.Instruction instruction) {
        assert instruction.operation == JMP
            || instruction.operation == NOP
        ;

        switch (instruction.operation) {
            case JMP:
                instruction.operation = NOP;
                break;
            case NOP:
                instruction.operation = JMP;
                break;
        }
    }
}
