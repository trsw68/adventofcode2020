package dev.xengil.aoc.day14;

import java.util.*;

public class Bitmask {

    public static final String BITMASK = "[01X]{36}";

    private final long ZEROES;

    private final long ONES;

    private final int[] floatingBits;

    public Bitmask(String bitmaskString) {
        assert bitmaskString.matches(BITMASK);

        StringBuilder zeroes = new StringBuilder();
        StringBuilder ones   = new StringBuilder();

        char[] chars = bitmaskString.toCharArray();

        for (char c : chars) {
            // zeroes
            zeroes.append(c == '0' ? 0 : 1);

            // ones
            ones.append(c == '1' ? 1 : 0);
        }

        List<Integer> floatingBits = new ArrayList<>();

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == 'X') {
                floatingBits.add(i);
            }
        }

        this.floatingBits = floatingBits.stream().mapToInt(Integer::intValue).toArray();

        this.ZEROES = Long.parseLong(zeroes.toString(), 2);
        this.ONES   = Long.parseLong(ones.toString(), 2);
    }

    public long apply(long value) {
        return (value & this.ZEROES) | this.ONES;
    }

    public long[] applyOnMemory(long address) {
        address |= this.ONES;

        Set<Long> addresses = new HashSet<>();

        addresses.add(address);

        for (int i : this.floatingBits) {
            Set<Long> permutations = new HashSet<>();

            addresses.forEach(addr -> {
                String base = "0".repeat(36);
                String perm = base.concat(Long.toString(addr, 2));
                char[] permutation  = perm.substring(perm.length() - 36).toCharArray();
                char[] permutationA = permutation.clone();
                char[] permutationB = permutation.clone();

                permutationA[i] = '0';
                permutationB[i] = '1';

                permutations.add(Long.parseLong(String.valueOf(permutationA), 2));
                permutations.add(Long.parseLong(String.valueOf(permutationB), 2));
            });

            addresses.addAll(permutations);
        }

        return addresses.stream().mapToLong(Long::longValue).toArray();
    }
}
