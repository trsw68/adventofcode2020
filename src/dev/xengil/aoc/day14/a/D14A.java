package dev.xengil.aoc.day14.a;

import dev.xengil.aoc.day14.MemoryInitializer;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class D14A {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD14"));

        MemoryInitializer memoryInitializer = new MemoryInitializer(input, true);

        System.out.println(memoryInitializer.sumValues());
    }
}
