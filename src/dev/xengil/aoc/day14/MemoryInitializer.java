package dev.xengil.aoc.day14;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MemoryInitializer {

    private static final String MASK = "mask = ";

    private static final String BITMASK = MASK + Bitmask.BITMASK;

    private static final String VALUE = "mem\\[(\\d+)] = (\\d+)";

    private final Map<Long, Long> memory;

    private Bitmask bitmask;

    public MemoryInitializer(List<String> input, boolean partA) {
        this.memory = new HashMap<>();

        if (partA) {
            input.forEach(this::execA);
        } else {
            input.forEach(this::execB);
        }
    }

    private void execA(String statement) {
        this.exec(statement, true);
    }

    private void execB(String statement) {
        this.exec(statement, false);
    }

    private void exec(String statement, boolean partA) {
        assert statement.matches(BITMASK) || statement.matches(VALUE);

        if (statement.matches(BITMASK)) {
            this.bitmask = new Bitmask(statement.substring(MASK.length()));
        } else {
            assert this.bitmask != null;

            Matcher matcher = Pattern.compile(VALUE).matcher(statement);

            if (!matcher.find()) {
                throw new IllegalStateException();
            }

            long  address = Long.parseLong(matcher.group(1));
            long value    = Long.parseLong(matcher.group(2));

            if (partA) {
                this.memory.put(address, this.bitmask.apply(value));
            } else {
                Arrays.stream(this.bitmask.applyOnMemory(address)).forEach(addr -> this.memory.put(addr, value));
            }
        }
    }

    public long sumValues() {
        return this.memory.values().stream().mapToLong(Long::longValue).sum();
    }
}
