package dev.xengil.aoc.day14.b;

import dev.xengil.aoc.day14.MemoryInitializer;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class D14B {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD14"));

        MemoryInitializer memoryInitializer = new MemoryInitializer(input, false);

        System.out.println(memoryInitializer.sumValues());
    }
}
