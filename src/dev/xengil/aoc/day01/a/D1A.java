package dev.xengil.aoc.day01.a;

import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class D1A {

    public static void main(String[] args) throws IOException {
        int value = Integer.parseInt(args[0]);
        List<Integer> input = InputConverter.getInstance().convertInput(Integer::parseInt, Path.of("rsc/inputD1"));

        int[] result = solve(value, input);

        System.out.println(result[0] * result[1]);
    }

    private static int[] solve(int value, List<Integer> input) {
        Set<Integer> history = new HashSet<>();

        for (int number : input) {
            int required = value - number;

            if (history.contains(required)) {
                return new int[] { required, number };
            }

            history.add(number);
        }

        throw new IllegalArgumentException("invalid input");
    }
}
