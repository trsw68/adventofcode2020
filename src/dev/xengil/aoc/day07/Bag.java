package dev.xengil.aoc.day07;

import java.util.*;

public class Bag {

    private static final Map<String, Bag> BAGS = new HashMap<>();

    public static Bag withColor(String color) {
        assert color != null;

        BAGS.computeIfAbsent(color, __ -> new Bag(color));

        return BAGS.get(color);
    }

    public static Set<Bag> getCreatedBags() {
        return Set.copyOf(BAGS.values());
    }

    private final String color;

    private Bag(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    public boolean canContain(Bag bag, Collection<BagRule> bagRules) {
        return this.canContain(bag, this.createBagRuleMap(bagRules));
    }

    public int countInnerBags(Collection<BagRule> bagRules) {
        return this.countInnerBags(this.createBagRuleMap(bagRules));
    }

    private boolean canContain(Bag bag, Map<Bag, BagRule> bagRules) {
        assert bagRules.containsKey(this) : "Invalid Map";
        assert bagRules.get(this).getOuterBag() == this : "Invalid Map";

        return bagRules
            .get(this)
            .getInnerBags()
            .stream()
            .anyMatch(innerBag -> innerBag == bag || innerBag.canContain(bag, bagRules))
        ;
    }

    private int countInnerBags(Map<Bag, BagRule> bagRules) {
        assert bagRules.containsKey(this) : "Invalid Map";
        assert bagRules.get(this).getOuterBag() == this : "Invalid Map";

        BagRule bagRule = bagRules.get(this);

        return bagRule
            .getInnerBags()
            .stream()
            .mapToInt(innerBag -> {
                int innerBagCount = bagRule.getInnerBagCount(innerBag);

                return innerBagCount + innerBagCount * innerBag.countInnerBags(bagRules);
            })
            .sum()
        ;
    }

    private Map<Bag, BagRule> createBagRuleMap(Collection<BagRule> bagRules) {
        Map<Bag, BagRule> bagRuleMap = new HashMap<>();

        bagRules.forEach(bagRule -> bagRuleMap.put(bagRule.getOuterBag(), bagRule));

        return bagRuleMap;
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.color, "Bag");
    }
}
