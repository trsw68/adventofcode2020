package dev.xengil.aoc.day07;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BagRule {

    public static BagRule fromString(String rule) {
        final String REGEX_A = "(([a-z ]+) bags contain (((\\d+) ([a-z ]+) bag[s]?(, (\\d+) ([a-z ]+) bag[s]?)*)|(no other bags)))\\.";
        final String REGEX_B = "((\\d+) ([a-z ]+) bag[s]?(, )?)";

        Pattern pattern = Pattern.compile(REGEX_A);
        Matcher matcher = pattern.matcher(rule);

        if (matcher.find()) {
            Bag               outerBag  = Bag.withColor(matcher.group(2));
            Map<Bag, Integer> innerBags = new HashMap<>();

            if (!matcher.group(1).equals("no other bags")) {
                String innerBagString = matcher.group(3);

                pattern = Pattern.compile(REGEX_B);
                matcher = pattern.matcher(innerBagString);

                while (matcher.find()) {
                    int count = Integer.parseInt(matcher.group(2));
                    Bag bag   = Bag.withColor(matcher.group(3));

                    innerBags.put(bag, count);
                }
            }

            return new BagRule(outerBag, innerBags);
        }

        throw new IllegalArgumentException();
    }

    private final Bag OUTER_BAG;

    private final Map<Bag, Integer> INNER_BAGS;

    public BagRule(Bag outerBag, Map<Bag, Integer> innerBags) {
        this.OUTER_BAG  = outerBag;
        this.INNER_BAGS = Map.copyOf(innerBags);
    }

    public Bag getOuterBag() {
        return OUTER_BAG;
    }

    public Set<Bag> getInnerBags() {
        return new HashSet<>(INNER_BAGS.keySet());
    }

    public int getInnerBagCount(Bag bag) {
        return this.INNER_BAGS.getOrDefault(bag, 0);
    }

    @Override
    public String toString() {
        return String.format(
            "BagRule: %s -> [%s]",
            OUTER_BAG.toString(),
            INNER_BAGS
                .keySet()
                .stream()
                .map(bag -> String.format("%d %ss", INNER_BAGS.get(bag), bag.toString()))
                .reduce((a, b) -> String.format("%s, %s", a, b))
                .orElse("")
        );
    }
}
