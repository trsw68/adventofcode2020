package dev.xengil.aoc.day07.b;

import dev.xengil.aoc.day07.Bag;
import dev.xengil.aoc.day07.BagRule;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class D7B {

    public static void main(String[] args) throws IOException {
        List<String>  input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD7"));
        List<BagRule> rules = input.stream().map(BagRule::fromString).collect(Collectors.toList());

        Bag providedBag = Bag.withColor(args[0]);

        long bagCount = providedBag.countInnerBags(rules);

        System.out.println(bagCount);
    }
}
