package dev.xengil.aoc.day06.a;

import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class D6A {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD6"));

        System.out.println(getGroupAnswers(input).stream().mapToInt(Set::size).reduce(0, Integer::sum));
    }

    private static List<Set<Character>> getGroupAnswers(List<String> input) {
        List<Set<Character>> groupAnswers        = new ArrayList<>();
        Set<Character>       currentGroupAnswers = new HashSet<>();

        input.add("");

        input.forEach(line -> {
            if (line.isEmpty()) {
                groupAnswers.add(Set.copyOf(currentGroupAnswers));
                currentGroupAnswers.clear();
                return;
            }

            for (char c : line.toCharArray()) {
                currentGroupAnswers.add(c);
            }
        });

        return groupAnswers;
    }
}
