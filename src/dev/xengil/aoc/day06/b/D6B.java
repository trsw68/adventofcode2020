package dev.xengil.aoc.day06.b;

import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class D6B {

    public static void main(String[] args) throws IOException {
        List<String> input = InputConverter.getInstance().convertInput(Path.of("rsc/inputD6"));

        System.out.println(getCommonGroupAnswerCount(input));
    }

    private static int getCommonGroupAnswerCount(List<String> input) {
        List<List<String>> groupInputs        = new ArrayList<>();
        List<String>       currentGroupInputs = new ArrayList<>();

        input.add("");

        input.forEach(line -> {
            if (line.isEmpty()) {
                groupInputs.add(new ArrayList<>(currentGroupInputs));
                currentGroupInputs.clear();
                return;
            }

            currentGroupInputs.add(line);
        });

        return groupInputs
            .stream()
            .mapToInt(group -> group
                .stream()
                .flatMapToInt(String::chars)
                .filter(answer -> group
                    .stream()
                    .map(String::chars)
                    .allMatch(otherAnswers -> otherAnswers.anyMatch(otherAnswer -> otherAnswer == answer))
                )
                .boxed()
                .collect(Collectors.toSet())
                .size()
            )
            .sum()
        ;
    }
}
