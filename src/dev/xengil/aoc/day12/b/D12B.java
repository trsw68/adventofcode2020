package dev.xengil.aoc.day12.b;

import dev.xengil.aoc.day12.NavigationInstruction;
import dev.xengil.aoc.day12.Ship;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class D12B {

    public static void main(String[] args) throws IOException {
        List<NavigationInstruction> input = InputConverter
            .getInstance()
            .convertInput(NavigationInstruction::fromString, Path.of("rsc/inputD12"))
        ;

        Ship ship = new Ship();

        input.forEach(ship::handleInstructionB);

        System.out.println(ship.getManhattanDistance());
    }
}
