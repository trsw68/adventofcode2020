package dev.xengil.aoc.day12;

import java.util.List;

public class Ship {

    private static final Position INITIAL_SHIP_POSITION = new Position(0, 0);

    private static final Position INITIAL_WAYPOINT_POSITION = new Position(-10, -1);

    private final Waypoint waypoint = new Waypoint();

    private char facingDirection = 'E'; // N, S, E, W

    private Position position = INITIAL_SHIP_POSITION;

    public int getManhattanDistance() {
        return this.position.getManhattanDistance();
    }

    public void handleInstructionA(NavigationInstruction instruction) {
        int  value     = instruction.VALUE;
        char direction = instruction.ACTION;

        switch (instruction.ACTION) {
            case 'F':
                direction = this.facingDirection;
            case 'N':
            case 'S':
            case 'E':
            case 'W':
                this.move(direction, value);
                break;
            case 'L':
                value = -value;
            case 'R':
                value /= 90;
                value = (value % 4 + 4) % 4;
                while (value-- > 0) this.rotate();
                break;
        }
    }

    public void handleInstructionB(NavigationInstruction instruction) {
        int value = instruction.VALUE;

        switch (instruction.ACTION) {
            case 'F':
                this.move(value);
                break;
            case 'N':
            case 'S':
            case 'E':
            case 'W':
                this.waypoint.move(instruction.ACTION, value);
                break;
            case 'L':
                value = -value;
            case 'R':
                value /= 90;
                value = (value % 4 + 4) % 4;
                while (value-- > 0) this.waypoint.rotate();
                break;
        }
    }

    private void move(char direction, int value) {
        this.position = this.position.translate(direction, value);
    }

    private void move(int value) {
        this.position = this.position.translate(this.waypoint.position.times(value));
    }

    private void rotate() {
        List<Character> directions = List.of('N', 'E', 'S', 'W');

        this.facingDirection = directions.get((directions.indexOf(this.facingDirection) + 1) % directions.size());
    }

    private static class Waypoint {

        private Position position = INITIAL_WAYPOINT_POSITION;

        private Waypoint() { }

        private void move(char direction, int value) {
            this.position = this.position.translate(direction, value);
        }

        private void rotate() {
            this.position = new Position(this.position.posNS, -this.position.posEW);
        }
    }

    private static class Position {

        private final int posEW;

        private final int posNS;

        private Position(int posEW, int posNS) {
            this.posEW = posEW;
            this.posNS = posNS;
        }

        private Position times(int value) {
            return new Position(this.posEW * value, this.posNS * value);
        }

        private Position translate(Position other) {
            return new Position(this.posEW + other.posEW, this.posNS + other.posNS);
        }

        private Position translate(char direction, int value) {
            int posEW = this.posEW;
            int posNS = this.posNS;

            switch (direction) {
                case 'N':
                    value = -value;
                case 'S':
                    posNS += value;
                    break;
                case 'E':
                    value = -value;
                case 'W':
                    posEW += value;
                    break;
            }

            return new Position(posEW, posNS);
        }

        private int getManhattanDistance() {
            return Math.abs(this.posEW) + Math.abs(this.posNS);
        }
    }
}
