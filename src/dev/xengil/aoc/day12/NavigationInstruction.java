package dev.xengil.aoc.day12;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NavigationInstruction {

    public static NavigationInstruction fromString(String s) {
        final String REGEX = "([NSEWLRF])(\\d+)";

        assert s.matches(REGEX) : "mismatch for string " + s;

        Matcher matcher = Pattern.compile(REGEX).matcher(s);

        matcher.find();

        return new NavigationInstruction(
            matcher.group(1).charAt(0),
            Integer.parseInt(matcher.group(2))
        );
    }

    public final char ACTION; // N, S, E, W, L, R, F

    public final int VALUE;

    public NavigationInstruction(char action, int value) {
        this.ACTION = action;
        this.VALUE  = value;
    }
}
