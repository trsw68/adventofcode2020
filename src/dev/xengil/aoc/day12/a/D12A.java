package dev.xengil.aoc.day12.a;

import dev.xengil.aoc.day12.NavigationInstruction;
import dev.xengil.aoc.day12.Ship;
import dev.xengil.aoc.util.InputConverter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class D12A {

    public static void main(String[] args) throws IOException {
        List<NavigationInstruction> input = InputConverter
            .getInstance()
            .convertInput(NavigationInstruction::fromString, Path.of("rsc/inputD12"))
        ;

        Ship ship = new Ship();

        input.forEach(ship::handleInstructionA);

        System.out.println(ship.getManhattanDistance());
    }
}
